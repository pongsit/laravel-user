<?php

// php artisan vendor:publish --provider="Pongsit\Firebase\FirebaseServiceProvider"

namespace Pongsit\User;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Pongsit\User\Providers\EventServiceProvider;
use Pongsit\User\Http\Middleware\UserCan;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->mergeConfigFrom(
        //     __DIR__.'/../config/services.php', 'services'
        // );

        // $this->app->register(EventServiceProvider::class); // For line socialite driver
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->publishes([
        //     __DIR__.'/../config/hi.php' => config_path('hi.php'),
        // ]);

        // $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->app['router']->namespace('Pongsit\\User\\Controllers')
                ->middleware(['web'])
                ->group(function () {
                    $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
                });
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'user');
        // $this->publishes([
        //     __DIR__.'/../public' => public_path('vendor/user'),
        //     __DIR__.'/../config/user.php' => config_path('user.php'),
        // ], 'public');
        $this->app['router']->aliasMiddleware('userCan', UserCan::class);
        // $this->app['router']->aliasMiddleware('own', Own::class);

        // $router = $this->app->make(Router::class);
        // $router->aliasMiddleware('firebase.auth', FirbaseAuth::class);
    }
}
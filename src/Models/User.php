<?php

namespace Pongsit\User\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Pongsit\Profile\Models\Profile;
use Pongsit\Role\Models\Role;
use Illuminate\Support\Facades\Auth;
use Pongsit\User\Database\Factories\UserFactory;

use Illuminate\Support\Str;

class User extends Model
{
    use HasFactory;
    // public $infos = array();

    protected static function newFactory()
    {
        return UserFactory::new();
    }

    // Disable Laravel's mass assignment protection
    protected $guarded = [];

    public $abilities;

    public $maxPowerRole;

    public $roleShow;

    public $userRole;

    /**
     * Boot the model.
     */
    // protected static function boot()
    // {
    //     parent::boot();

    //     static::created(function ($user) {
    //         $user->slug = $this->createSlug();
    //         $user->save();
    //     });
    // }

   //  public function createSlug(){
   //      $name = $this->name;
   //      if (static::whereSlug($slug = Str::slug($name))->exists()) {


   //       $max = static::whereName($name)->latest('id')->skip(1)->value('slug');

   //       if(!empty($max)){
   //           if (is_numeric($max[-1])) {

   //              return preg_replace_callback('/(\d+)$/', function ($mathces) {

   //                 return $mathces[1] + 1;

   //              }, $max);

   //           }
   //       }
   //       return "{$slug}-2";
   //    }
   //    return $slug;
   // }
    
    public function roles() {
        return $this->belongsToMany('Pongsit\Role\Models\Role')->withTimestamps();
    }

    public function payments() {
        return $this->hasMany('Pongsit\Payment\Models\Payment');
    }
    
    public function profile() {
        return $this->hasOne('Pongsit\Profile\Models\Profile');
    }

    public function notes(){
        return $this->morphMany('Pongsit\Note\Models\Note', 'notable');
    }

    public function getPayment(){
        if(empty($this->infos['id'])){
            $user = Auth::user();
            if(empty($user)){
                return [];
            }
            $user_id = $user->id;
        }else{
            $user_id = $this->infos['id'];
        }

        $user = $this->with('payments')->find($user_id);
        return $user->payments->toArray();
    }

    // public function getInfosAttribute(){
    //     return $this->getInfo();
    // }

    public function getInfoAttribute(){
        return (object) $this->getInfo();
    }

    // public function getAvatarAttribute(){
    //     return $this->avatar;
    // }

    public function getGenderShowAttribute(){
        $gender_show = '';
        switch($this->getGenderAttribute()){
            case 'male': 
                $gender_show = 'ชาย'; 
                break;
            case 'female': 
                $gender_show = 'หญิง'; 
                break;
            case 'no':
                $gender_show = 'ไม่ระบุ'; 
                break;
        }
        return $gender_show;
    }

    // public function getRoleShowAttribute(){
    //     $role_shows = $this->roles->map(function($vs){
    //         return config('role.names')[$vs['role']];
    //     });
    //     if(empty($role_shows)){
    //         return [];
    //     }
    //     return implode(', ',$role_shows->all());
    // }

    public function getGenderAttribute(){
        if(!empty($this->profile->gender)){
            return $this->profile->gender;
        }
    }

    public function getRoleShowAttribute(){
        $roleShow = '';
        if(!isset($this->roleShow)){
            $comma = '';
            if(!empty($this->getUserRole())){
                foreach($this->getUserRole() as $vs){
                    $roleShow .= $comma.$vs->name_show;
                    $comma = ', ';
                }
                $this->roleShow = $roleShow;
            }else{
                $this->roleShow = '';
            }
        }
        return $this->roleShow;
    }

    public function getStatusShowAttribute(){
        $status = '';
        switch($this->status){
            case 1: 
                $status = 'ปกติ'; 
                break;
            case 0: 
                $status = 'ยกเลิก'; 
                break;
        }
        return $status;
    }

    public function getFirstnameAttribute(){
        if(!empty($this->profile->firstname)){
            return $this->profile->firstname;
        }
    }

    public function getLastnameAttribute(){
        if(!empty($this->profile->lastname)){
            return $this->profile->lastname;
        }
    }

    public function getPhoneAttribute(){
        if(!empty($this->profile->phone)){
            return $this->profile->phone;
        }
    }

    public function getAboutMeAttribute(){
        if(!empty($this->profile->aboutme)){
            return $this->profile->aboutme;
        }
    }

    // public function getIsAdminAttribute(){
    //     $count = $this->roles->map(function($vs){
    //         if($vs['role'] = 'admin'){
    //             return 1;
    //         }
    //     })->sum();
    //     if($count > 0){
    //         return true;
    //     }
    //     return false;
    // }

    // public function own($infos){
    //     if(session('user')['id'] != $infos['user_id']){
    //         return 0;	
    //     }
    //     return 1;
    // }

    // public function getStudents(){
    //    // dd($this->with('roles')->get());
    //     // $users = $this->with('roles')->filter(function($vs){		
    //     //     dd($vs);	
    //     // });
    //     return $this->with('profile')->get()->filter(function($vs){
    //         if($vs->roles()->where('role', 'student')->count() > 0){
    //             return true;
    //         }else{
    //             return false;
    //         }
    //     });
    //     // return $users;
    // }
    
    // public function getInfo(){

    //     $user = $this;

    //     $user_infos = $user->toArray();

    //     $user_infos['genderIsSecret'] = 0; 
    //     $user_infos['genderIsMale'] = 0; 
    //     $user_infos['genderIsFemale'] = 0; 
    //     if($user->profile){
    //         $user_profile = $user->profile->toArray();
    //         if(!empty($user_profile)){ 
    //             switch($user_profile['gender']){
    //                 case 'no': 
    //                     $user_profile['genderIsSecret'] = 1; 
    //                     $user_profile['gender'] = 'ไม่ระบุ'; 
    //                     break;
    //                 case 'male': 
    //                     $user_profile['genderIsMale'] = 1; 
    //                     $user_profile['gender'] = 'ชาย'; 
    //                     break;
    //                 case 'female': 
    //                     $user_profile['genderIsFemale'] = 1; 
    //                     $user_profile['gender'] = 'หญิง'; 
    //                     break;
    //             }
    //             $user_infos = array_merge($user_profile,$user_infos);
    //         }
    //     }

    //     $userRole = $user->roles->toArray();
    //     $user_infos['roleShow'] = '';

    //     foreach(role()->getAll() as $k => $vs){
    //         $user_infos['is'.ucfirst($vs['name'])] = 0;
    //     }

    //     $user_infos['roles'] = array();
    //     if(!empty($userRole)){
    //         $comma = '';
    //         foreach($userRole as $vs){
    //             $user_infos['is'.ucfirst($vs['name'])] = 1;
    //             if($user_infos['is'.ucfirst($vs['name'])] == 1){
    //                 $user_infos['roles'][] = $vs['name'];
    //             }
    //             $user_infos['roleShow'] .= $comma.$vs['name_show'];
    //             $comma = ', ';
    //         }
    //         $userRole = array_merge($user_infos,$userRole);
    //     }

    //     switch($user_infos['status']){
    //         case 1: 
    //             $user_infos['status_show'] = 'ปกติ'; 
    //             break;
    //         case 0: 
    //             $user_infos['status_show'] = 'ยกเลิก'; 
    //             break;
    //     }

    //     $user_infos['user_id'] = $user_infos['id'];

    //     return $user_infos;
    // }
                
    public function getUsers(){
        $users = \App\Models\User::all()->map(function($user){			
            return $this->getUser($user->id);
        });
        return $users;
    }

    public function getNumberOfUser(){
        $n = \App\Models\User::all()->count();
        return $n;
    }

    public function isLogin(){
        if(!empty(session('user'))){
            if(session('user')->id == $this->id){
                return true;
            }
        }
        return false;
    } 

    public function getMaxPowerRole(){
        if(!isset($this->maxPowerRole)){
            if(!$this->getUserRole()->isEmpty()){
                $maxPowerRole = $this->getUserRole()->sortByDesc('power')->first();
                $this->maxPowerRole = $maxPowerRole;
            }else{
                $this->maxPowerRole = new Role();
            }
        }
        return $this->maxPowerRole;
    }

    public function isAdmin(){
        if(!$this->isLogin()){
            return false;
        }else{
            if(empty($this->infos)){
                if(session('user')['isAdmin']){
                    return true;
                }
            }else{
                if($this->infos['isAdmin']){
                    return true;
                }
            }
        }
        return false;
    } 

    public function getSession(){
        if($this->isLogin()){
            return session('user');
        }
        return [];
    }

    // public function id(){
    //     if($this->isLogin()){
    //         return session('user')['id'];
    //     }
    //     return false;
    // }

    // public function withFullInfo($user_id){
    //     $this_user = $this->find($user_id);
    //     $this_user->infos = $this->getInfo($user_id);
    //     return $this_user;
    // }

    public function getUserRole()
    {
        if(!isset($this->userRole)){
            $this->userRole = $this->roles()->get();
        }

        return $this->userRole;
    }

    public function hasRole($role_name){

        if($this->getUserRole()->contains('name',$role_name)){
            return true;
        }

        return false;
    }

    public function role_is($role_name){
        if($this->roles->where('name',$role_name)->count() > 0){
            return true;
        }

        return false;
    }

    public function roleIs($role_name){
        return $this->role_is($role_name);
    }

    public function getAbilitiesAttribute(){
        $abilities = array();
        foreach($this->roles as $v){
            $abs = $v->abilities()->get();
            foreach($abs as $_v){
                $abilities[] = $_v->slug;
            }
        }
        $this->abilities = $abilities;
        return $abilities;
    }

    public function can($ability_slug){
        if(user()->id == 1){
            return true;
        }
        if(!isset($this->abilities)){
            $this->abilities = $this->getAbilitiesAttribute();
        }
        if(!empty($this->abilities)){
            if(in_array($ability_slug,$this->abilities)){
                return true;
            }
        }
        return false;
    }
}
            
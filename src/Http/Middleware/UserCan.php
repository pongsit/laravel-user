<?php

namespace Pongsit\User\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class UserCan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $ability)
    {
        if(user()->can($ability)){
            return $next($request);
        }
        return abort(403);
    }
}

<?php

namespace Pongsit\User\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Pongsit\User\Models\User;
use Cookie;
use Laravel\Socialite\Facades\Socialite;
use Pongsit\Role\Models\Role;
use Pongsit\System\Models\System;
use Pongsit\Note\Models\Note;
use Throwable;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

use Illuminate\Support\Facades\Url;

class UserController extends Controller
{
    public function index(){
		$variables = [];
        $append_query_strings = [];

        // การจัดเรียงข้อมูล
        $order_by = 'created_at';
        if(!empty($_GET['order_by'])){
            $order_by = $_GET['order_by'];
            $append_query_strings['order_by'] = 'order_by='.$_GET['order_by'];
        }
        $variables['order_by'] = $order_by;

        $order_by_show = '';
        switch ($order_by) {
            case 'name':
                $order_by_show = 'ชื่อแสดง';
                $sort_shows['desc'] = 'ย้อนตัวอักษร';
                $sort_shows['asc'] = 'ตามตัวอักษร';
                break;
            case 'created_at':
                $order_by_show = 'เวลาเข้าร่วม';
                $sort_shows['desc'] = 'จากใหม่ไปเก่า';
                $sort_shows['asc'] = 'จากเก่าไปใหม่';
                break;
            case 'email':
                $order_by_show = 'อีเมล';
                $sort_shows['desc'] = 'ย้อนตัวอักษร';
                $sort_shows['asc'] = 'ตามตัวอักษร';
                break;
            case 'status':
                $order_by_show = 'สถานะ';
                $sort_shows['desc'] = 'ปกติขี้นก่อน';
                $sort_shows['asc'] = 'ยกเลิกขี้นก่อน';
                break;
            default:
                $order_by_show = 'รหัส';
                $sort_shows['desc'] = 'จากมากไปน้อย';
                $sort_shows['asc'] = 'จากน้อยไปมาก';
                break;
        }

        $sort = 'desc';
        $append_query_strings['sort'] = 'sort='.$sort;
        $sort_show = $sort_shows[$sort];
        if(!empty($_GET['sort'])){
            $sort = $_GET['sort'];
            $append_query_strings['sort'] = 'sort='.$_GET['sort'];
            $sort_show = $sort_shows[$sort];
        }
        $variables['sort_reverse'] = '';
        if($append_query_strings['sort']!='sort=asc'){
            $variables['sort_reverse']='sort=asc';
        }else{
            $variables['sort_reverse']='sort=desc';
        }
        $variables['sort'] = $sort;

        $variables['sort'] = $sort;
        $variables['sort_show'] = $sort_show;

        $order_by_show .= $sort_show;
        $variables['order_by_show'] = $order_by_show;

        // ข้อมูลเฉพาะคำที่ Search
        $search = '';
        if(!empty($_GET['search'])){
            $search = $_GET['search'];
            $append_query_strings['search'] = 'search='.$_GET['search'];
        }
        $variables['search'] = $search;

        // ข้อมูลเฉพาะ User
        if(!empty($_GET['user_id'])){
            $user_id = $_GET['user_id'];
        }

        $variables['append_query_strings'] = $append_query_strings;

        $item_per_page = 30;
        if(empty($search)){
            $variables['members'] = User::orderBy($order_by,$sort)->paginate($item_per_page);
        }else{
            $variables['members'] = User::where('name','like','%'.$search.'%')
                ->orWhere('email','like','%'.$search.'%')
                ->orWhere('created_at','like','%'.$search.'%')
                ->orderBy($order_by,$sort)
                ->paginate($item_per_page);
        }

        $variables['paginations'] = $variables['members']->toArray();

        $variables['count_result'] = $variables['paginations']['total'];

        // dd($variables['paginations']);
        $variables['paginations']['append_query_string'] = '';
        if(!empty($append_query_strings)){
            $variables['paginations']['append_query_string'] = '&'.implode('&', $append_query_strings);
        }
        $variables['pagination'] = view('system::components.pagination',$variables['paginations']);
        
        $variables['order_number'] = 1;
        if(!empty($_GET['page'])){
            $variables['order_number'] = ($_GET['page']-1)*$item_per_page+1;
        }

        $variables['currentUrl'] = url()->current();

        if(view()->exists('user.index')){
			return view('user.index',$variables);
		}else{
			return view('user::index',$variables);
		}
	}

	// public function register(){
	// 	$infos['pageName'] = $_ENV['SITE_NAME'].' - ลงทะเบียน';
	// 	return view('index',$infos);
	// }

	// public function login(){
	// 	$infos['pageName'] = $_ENV['SITE_NAME'].' - เข้าสู่ระบบ';

	// 	if(!user()->isLogin()){
	// 		return view('login');
	// 	}else{
	// 		return redirect('/')->with(['success'=>'ยินดีต้อนรับเข้าสู่ระบบ']);
	// 	}
	// }

	public function update(Request $request, User $user){
		
		$inputs = $request->all();

		if(!user()->can('manage_user')){
			if(!user()->can('change_own_account_info')){
				return back()->with(['error'=>'คุณไม่มีสิทธิ์ในการดำเนินการนี้ครับ']);
			}
			$user = user();
		}

		$request->validate([
			'name' => 'required',
			'email' => [
		        'required',
		        'email',
		        Rule::unique('users')->ignore($user->id),
		    ],
		],[
			'name.required'=>'กรุณากรอกชื่อแสดง',
			'email.required'=>'กรุณากรอกอีเมล',
			'email.unique'=>'อีเมลนี้มีเจ้าของแล้ว',
			'email.email'=>'อีเมลนี้ไม่สามารถใช้ได้',
		]);

		unset($inputs['_token']);

		if(!empty($inputs['password'])){
			$request->validate([
				'password' => ['required', 'confirmed', Password::min(8)]
			],[
				'password.required'=>'กรุณากรอกรหัสผ่าน',
				'password.confirmed'=>'กรุณายืนยันรหัสผ่านให้ตรงกัน',
				'password.min'=>'รหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวอักษร',
			]);

			$user->password = Hash::make($inputs['password']);
		}

		if(isset($inputs['status'])){
			if(user()->can('manage_user')){
				if($user->id != 1){
					$user->status = $inputs['status'];
				}
			}else{
				return back()->with(['error'=>'คุณไม่มีสิทธิ์ในการดำเนินการนี้ครับ']);
			}
		}

		if(!empty($inputs['name'])){
			if($user->name != $inputs['name']){
				$user->name = $inputs['name'];
				$user->slug = slug($user,'name');
			}
		}

		if($inputs['email'] != $user->email){
			$user->email = $inputs['email'];
		}

		$user->save();

		if(user()->id == $user->id){
			session(['user'=>user($user->id)]);
			return redirect(route('profile'))->with(['success'=>'ปรับปรุงข้อมูลเรียบร้อย']);
		}

		if(!empty($request->back_route_name)){
			return redirect(route($request->back_route_name,$user->slug))->with(['success'=>'ปรับปรุงข้อมูลเรียบร้อย']);
		}else{
			return redirect(route('profile.user',$user->slug))->with(['success'=>'ปรับปรุงข้อมูลเรียบร้อย']);
		}
	}

	public function updateUserRole(Request $request, User $user){

		// $request->validate([
		// 	'user_id' => ['required'],
		// 	'role_id' => ['required']
		// ],[
		// 	'user_id.required'=>'กรุณาติดต่อผู้ดูแลระบบ เพื่อตรวจสอบ user id',
		// 	'role_id.required'=>'กรุณาเลือกบทบาทที่ต้องการ',
		// ]);

		if(!user()->can('update_user_role') && user()->getMaxPowerRole()->power < $user->getMaxPowerRole()->power){
			return back()->with(['error'=>'คุณไม่มีสิทธิ์ดำเนินการครับ']);
		}

		$inputs = $request->all();	

		if($user->id == 1){
			if(!empty($inputs['role_id'])){
				if(!in_array(1,$inputs['role_id'])){
					$inputs['role_id'][] = 1;
				}
			}else{
				$inputs['role_id'][] = 1;
			}
		}

		if(!empty($inputs['role_id'])){
			$user->roles()->sync($inputs['role_id']);
		}else{
			$user->roles()->sync(array());
		}

		if(user()->id == $user->id){
			session(['user'=>user($user->id)]);
		}

		return back()->with(['success'=>'ปรับปรุงข้อมูลเรียบร้อย']);
	}

	public function noteInsert(Request $request, User $user){

		// $request->validate([
		// 	'user_id' => ['required'],
		// 	'role_id' => ['required']
		// ],[
		// 	'user_id.required'=>'กรุณาติดต่อผู้ดูแลระบบ เพื่อตรวจสอบ user id',
		// 	'role_id.required'=>'กรุณาเลือกบทบาทที่ต้องการ',
		// ]);

		if(!user()->can('manage_user')){
			return back()->with(['error'=>'คุณไม่มีสิทธิ์ดำเนินการครับ']);
		}

		$inputs = $request->all();	
		$user->notes()->save(new Note(['message' => $request->message,'user_id'=>user()->id]));

		return back()->with(['success'=>'ปรับปรุงข้อมูลเรียบร้อย']);
	}

	public function userSearch(Request $request){
		$inputs = $request->all();

		$results = User::where('name','like','%'.$inputs['search'].'%')
            ->orWhere('email','like','%'.$inputs['search'].'%')
            ->paginate(10);
		return Response($results);
	}

	public function create(){
        $variables['user'] = user();
        return view('user::create',$variables);
    }

    public function show(User $user){
        if(!user()->isLogin()){
			return redirect()->route('login');
		}

        $variables = [];
		if(user()->can('manage_user') && !empty($user->id)){
    		$variables['user'] = $user;
    	}else{
    		$variables['user'] = user();	
    	}

    	$variables['own'] = 0;
    	if(user()->id == $variables['user']->id){
    		$variables['own'] = 1;
    	}

		if(view()->exists('user.show')){
			return view('user.show',$variables);
		}else{
			return view('user::show',$variables);
		}
    }

    public function store(Request $request){
    	$request->validate([
			'name' => 'required',
			'email' => [
		        'required',
		        'unique:users',
		        'email',
		    ],
		],[
			'name.required'=>'กรุณากรอกชื่อแสดง',
			'email.required'=>'กรุณากรอกอีเมล',
			'email.unique'=>'อีเมลนี้มีเจ้าของแล้ว',
			'email.email'=>'อีเมลนี้ไม่สามารถใช้ได้',
		]);

		$request->validate([
			'password' => ['required', 'confirmed', Password::min(8)]
		],[
			'password.required'=>'กรุณากรอกรหัสผ่าน',
			'password.confirmed'=>'กรุณายืนยันรหัสผ่านให้ตรงกัน',
			'password.min'=>'รหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวอักษร',
		]);

    	$inputs = $request->all();
    	$inputs['password'] = bcrypt($inputs['password']);
    	unset($inputs['password_confirmation']);
    	$user = new User($inputs);
    	$user->slug = slug($user,'name');
    	$user->save();

        return redirect()->route('profile.user',$user->slug)->with(['success'=>'สร้างผู้ใช้ใหม่เรียบร้อย']);
    }
}

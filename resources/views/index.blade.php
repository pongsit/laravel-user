@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row mb-sm-0 sticky-top bg-dark" >
        <div class="col pt-2">
            <div class="d-none d-lg-block mb-0 mt-0 " style="position:relative;min-height: 70px; padding-bottom: 10px;">     
                <div class="font-italic flex-fill d-flex">
                    <div class="me-2">ขณะนี้เรียงตาม: {{$order_by_show}}</div>
                    <div> | </div>
                    <div class="ms-2">
                        จัดการ: <a href="{{route('user.create')}}">
                                    <i style="font-size: 0.7em;" class="fas fa-plus-circle"></i> เพิ่มรายการ 
                                </a>
                    </div>
                </div>
                <div class="" >
                    <form method="get">
                         <div class="d-flex">
                            <div class="" style="margin-right: 5px; width: 300px;">
                                <input class="form-control mr-sm-2 bg-dark text-white" type="search" name="search" placeholder="ค้นหา">
                            </div>
                            <div style="position:relative; top:3px;">
                                <button class="btn btn-outline-secondary" type="submit">ค้นหา</button> 
                            </div>
                        </div>
                    </form>
                    @if(!empty($search))
                    <div>
                        <span class="text-warning"><u>แสดงเฉพาะที่เกี่ยวกับ</u>:</span> {{$search}} <a href="{{$currentUrl}}">[ยกเลิก]</a> 
                    </div>
                    @endif
                </div>
                <div class="rounded" style="padding: 0 10px; border: solid 2px grey; position:absolute; top:5px; right:0;">
                    @if(!empty($search)) ค้นพบ: @else สมาชิก: @endif
                    <span style="font-size:1.5em;">{{$count_result}}</span> คน
                </div>
            </div>
            <div class="row d-none d-lg-block mb-0 bg-dark border-bottom border-top border-secondary">
                <div class="col">
                    <div class="d-sm-flex">
                        <div class="d-inline-block">
                            <div class="d-none d-sm-block mr-2" style="width:60px;">
                                ลำดับ
                            </div>
                        </div>
                        <div class="d-inline-block">
                            <div class="text-sm-left" style="width:150px;">
                                <a href="
                                        @php 
                                            $_name = 'created_at';
                                            $append_query_strings['order_by']='order_by='.$_name; 
                                            $append_query_strings['sort']=$sort_reverse; 
                                            echo $currentUrl.'?'.implode('&', $append_query_strings);  
                                        @endphp
                                    ">
                                    เวลาเข้าร่วม 
                                    @if($order_by=='created_at' && $sort=='desc') 
                                        <i class="fas fa-arrow-up" style="font-size:0.6em;"></i> 
                                    @endif
                                    @if($order_by=='created_at' && $sort=='asc') <i class="fas fa-arrow-down" style="font-size:0.6em;"></i> @endif
                                </a>
                            </div>
                        </div>
                        <div class="d-inline-block mr-2 pl-2 border-left border-secondary" style="width:250px;">
                            <a href="
                                    @php 
                                        $_name = 'name';
                                        $append_query_strings['order_by']='order_by='.$_name; 
                                        $append_query_strings['sort']=$sort_reverse; 
                                        echo $currentUrl.'?'.implode('&', $append_query_strings);  
                                    @endphp
                                ">
                                ชื่อแสดง 
                                @if($order_by=='name' && $sort=='desc') <i class="fas fa-arrow-up" style="font-size:0.6em;"></i> @endif
                                @if($order_by=='name' && $sort=='asc') <i class="fas fa-arrow-down" style="font-size:0.6em;"></i> @endif
                            </a>
                        </div>
                        <div class="d-inline-block flex-fill mr-2 pl-2 border-left border-secondary">
                            <a href="
                                    @php 
                                        $_name = 'email';
                                        $append_query_strings['order_by']='order_by='.$_name; 
                                        $append_query_strings['sort']=$sort_reverse; 
                                        echo $currentUrl.'?'.implode('&', $append_query_strings);  
                                    @endphp
                                ">
                                Email 
                                @if($order_by=='email' && $sort=='desc') <i class="fas fa-arrow-up" style="font-size:0.6em;"></i> @endif
                                @if($order_by=='email' && $sort=='asc') <i class="fas fa-arrow-down" style="font-size:0.6em;"></i> @endif
                            </a>
                        </div>
                        <div class="d-inline-block border-left border-secondary">
                            <div class="text-sm-right" style="width:70px;">
                                <a href="
                                    @php 
                                        $_name = 'status';
                                        $append_query_strings['order_by']='order_by='.$_name; 
                                        $append_query_strings['sort']=$sort_reverse; 
                                        echo $currentUrl.'?'.implode('&', $append_query_strings);  
                                    @endphp
                                ">
                                    สถานะ 
                                    @if($order_by=='status' && $sort=='desc') <i class="fas fa-arrow-up" style="font-size:0.6em;"></i> @endif
                                    @if($order_by=='status' && $sort=='asc') <i class="fas fa-arrow-down" style="font-size:0.6em;"></i> @endif
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-lg-none pb-2">
                <div class="row justify-content-center mb-1">
                    <div style="position:relative; border: solid 2px grey; margin: 0 10px;" class="col text-center rounded">
                        <div style="position:absolute; top:5px; left:15px;">@if(!empty($search)) ค้นพบ: @else สมาชิก: @endif</div>
                        <div style="overflow:scroll; position:relative; top:5px; font-size:70px; line-height:100px;">{{$count_result}}</div>
                    </div>
                </div>
                <div>
                    เลือกให้เรียงตาม: 
                    <a class="text-nowrap" href="
                            @php 
                                $_name = 'created_at';
                                $append_query_strings['order_by']='order_by='.$_name; 
                                $append_query_strings['sort']=$sort_reverse; 
                                echo $currentUrl.'?'.implode('&', $append_query_strings);  
                            @endphp
                        ">
                        เวลาเข้าร่วม
                    </a> | 
                    <a class="text-nowrap" href="
                            @php 
                                $_name = 'name';
                                $append_query_strings['order_by']='order_by='.$_name; 
                                $append_query_strings['sort']=$sort_reverse; 
                                echo $currentUrl.'?'.implode('&', $append_query_strings);  
                            @endphp
                        ">
                        ชื่อแสดง
                    </a> | 
                    <a class="text-nowrap" href="
                            @php 
                                $_name = 'email';
                                $append_query_strings['order_by']='order_by='.$_name; 
                                $append_query_strings['sort']=$sort_reverse; 
                                echo $currentUrl.'?'.implode('&', $append_query_strings);  
                            @endphp
                        ">
                        Email
                    </a> | 
                    <a class="text-nowrap" href="
                            @php 
                                $_name = 'status';
                                $append_query_strings['order_by']='order_by='.$_name; 
                                $append_query_strings['sort']=$sort_reverse; 
                                echo $currentUrl.'?'.implode('&', $append_query_strings);  
                            @endphp
                        ">
                        สถานะ
                    </a> 
                </div>
                <div class="font-italic">ขณะนี้เรียงตาม: {{$order_by_show}}</div>
                <div class="">จัดการ: 
                    <a href="{{route('user.create')}}"><i style="font-size: 0.7em;" class="fas fa-plus-circle"></i> เพิ่มรายการ</a>
                </div>
                <div class="mt-3 mt-sm-0 mr-sm-2">
                    <form method="get">
                         <div class="d-flex">
                            <div class="flex-fill" style="margin-right: 5px;">
                                <input class="form-control mr-sm-2 bg-dark text-white" type="search" name="search" placeholder="ค้นหา">
                            </div>
                            <div class="ml-auto" style="position:relative; top:3px;">
                                <button class="btn btn-outline-secondary" type="submit">ค้นหา</button> 
                            </div>
                        </div>
                    </form>
                </div>
                @if(!empty($search))
                    <div>
                        <span class="text-warning"><u>แสดงเฉพาะที่เกี่ยวกับ</u>:</span> {{$search}} <a href="{{$currentUrl}}">[ยกเลิก]</a> 
                    </div>
                @endif
            </div>
        </div>
    </div>

    @php
        $bg = '';
        $bg_mem = $bg; 
    @endphp

    @foreach($members as $vs)
        <div class="row mb-sm-0" style="{{$bg}}">
            <div class="col">
                <div class="d-lg-flex">
                    <div class="d-inline-block">
                        <div class="d-inline d-lg-block mr-2" style="width:60px;">
                            {{$order_number}}.
                        </div>
                    </div>
                    <div class="d-inline-block" style="width:150px;">
                        @php 
                            $velas = explode(' ',vela()->thai($vs->created_at));
                            $times = explode(' ',$vs->created_at); 
                            echo '<a href="'.$currentUrl.'?search='.$times[0].'">'.$velas[0].' '.$velas[1].'</a> '.$velas[2];
                        @endphp
                    </div>
                    <div class="mr-2">
                        <div class="d-none d-lg-inline-block border-left border-secondary pl-2" style="width:250px;">
                            <div style="width:100%;">
                                <a target="_blank" href="{{url('profile/'.$vs->slug)}}">{{$vs->name}}</a>
                            </div>
                        </div>
                        <div class="d-lg-none" >
                            <div class="d-flex">
                                <div style="width:100px;">ชื่อแสดง:</div>
                                <div><a target="_blank" href="{{url('profile/'.$vs->slug)}}">{{$vs->name}}</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="mr-2 flex-fill">
                        <div class="d-none d-lg-inline-block border-left border-secondary pl-2">
                            <div style="width:100%;">
                                {{$vs->email}}
                            </div>
                        </div>
                        <div class="d-lg-none" >
                            <div class="d-flex">
                                <div style="width:100px;">Email:</div>
                                <div>{{$vs->email}}</div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="d-none d-lg-inline-block border-left border-secondary">
                            <div class="text-right" style="width:70px;">
                                @switch($vs->status)
                                    @case('1') 
                                        {{'ปกติ'}} 
                                        @break
                                    @case('0') 
                                        {{'ยกเลิก'}} 
                                        @break
                                @endswitch
                            </div>
                        </div>
                        <div class="d-lg-none" >
                            <div class="d-flex">
                                <div style="width:100px;">สถานะ:</div>
                                <div>
                                    @switch($vs->status)
                                        @case('1') 
                                            {{'ปกติ'}} 
                                            @break
                                        @case('0') 
                                            {{'ยกเลิก'}} 
                                            @break
                                    @endswitch
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @php 
            $order_number++;
            if(!empty($bg)){
                $bg = $bg_mem;
            }else{
                $bg = 'background-color:rgb(33,37,41,0.6);';
            }
        @endphp
    @endforeach
    @php
        echo $pagination;
    @endphp
</div>
@stop
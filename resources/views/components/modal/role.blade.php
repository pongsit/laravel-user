@props(['user'=>$user])

<div class="modal fade" id="roleEdit" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">บทบาท</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{route('user.role.update',$user->slug)}}">
            @csrf
            @foreach(role()->where('status',1)->orderBy('power','desc')->get() as $vs)
                @if($vs->power > user()->getMaxPowerRole()->power && user()->id != 1) @continue @endif
                <div class="form-check d-inline-block">
                    <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" name="role_id[]" value="{{$vs->id}}"
                        @if($user->role_is($vs->name)) checked @endif>
                        {{$vs->name_show}}
                    </label>
                </div>
            @endforeach
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>
        <button type="button" class="btn btn-primary roleEdit-submit">ส่งข้อมูล</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(function () {
        $('body').on('click', '.roleEdit-submit', function (e) {
            $('#roleEdit').find('form').submit();
            $('#roleEdit').modal('hide');
        });
    });
</script>
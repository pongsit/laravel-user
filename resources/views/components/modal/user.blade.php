@props(['id','title'])

<div class="modal fade" id="{{$id}}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{$title}}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('user.store')}}">
                    @csrf
                    <div class="d-flex">
                        <div style="width:150px;">ชื่อแสดง</div>
                        <input class="form-control mb-2" placeholder="ชื่อแสดง" type="text" name="name">
                    </div>
                    <div class="d-flex">
                        <div style="width:150px;">อีเมล</div>
                        <input class="form-control mb-2" placeholder="อีเมล" type="email" name="email">
                    </div>
                    <div class="d-flex">
                        <div style="width:150px;">รหัสผ่าน</div>
                        <input class="form-control mb-2" placeholder="รหัสผ่าน" type="password" name="password">
                    </div>
                    <div class="d-flex">
                        <div style="width:150px;">ยืนยันรหัสผ่าน</div>
                        <input class="form-control mb-2" placeholder="ยืนยันรหัสผ่าน" type="password" name="password_confirmation">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary {{$id}}-submit">ส่งข้อมูล</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('body').on('click', '.{{$id}}-submit', function (e) {
            $('#{{$id}}').find('form').submit();
            $('#{{$id}}').modal('hide');
        });
    });
</script>
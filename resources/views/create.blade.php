@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-lg-8 mb-3">
            <div class="card shadow">
                <div class="card-header pt-3 pb-3 d-flex justify-content-between">
                    <div>
                        <img style="width: 120px;height: auto;" src="{{asset('img/logo.png')}}">
                    </div>
                    <div class="ms-4">
                       สร้าง User ใหม่
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{route('user.store')}}">
                        @csrf
                        <div class="d-flex">
                            <div style="width:150px;">ชื่อแสดง</div>
                            <input class="form-control bg-dark text-white mb-2" placeholder="ชื่อแสดง" type="text" name="name">
                        </div>
                        <div class="d-flex">
                            <div style="width:150px;">อีเมล</div>
                        <input class="form-control bg-dark text-white mb-2" placeholder="อีเมล" type="email" name="email">
                        </div>
                        <div class="d-flex">
                            <div style="width:150px;">รหัสผ่าน</div>
                        <input class="form-control bg-dark text-white mb-2" placeholder="รหัสผ่าน" type="password" name="password">
                        </div>
                        <div class="d-flex">
                            <div style="width:150px;">ยืนยันรหัสผ่าน</div>
                        <input class="form-control bg-dark text-white mb-2" placeholder="ยืนยันรหัสผ่าน" type="password" name="password_confirmation">
                        </div>
                        <div class="mt-3 d-flex justify-content-end">
                            <input type="submit" class="col-2 px-4 bg-dark text-white rounded text-center" value="ส่ง">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
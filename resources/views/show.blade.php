@extends('layouts.main')

@section('content')

<x-profile::updateAvatar :user="$user" />

<div class="container">
    <div class="row">
        <div class="col-12 col-md-3">
            <div class="d-flex justify-content-center">
                <div class="mt-4 m-2" style="width:150px;height:150px;">
                    <div style="
                        position: relative;
                        background-image: url(
                            {{asset('/profile/show/avatar/'.$user->slug.'/md/').'/'.$user->avatar.'?v='.session()->get('imgVersion') ?? 1 }}
                        ); 
                        background-repeat:no-repeat; 
                        background-size: cover; 
                        width:100%;
                        padding-top:100%;
                        border-radius: 50%;";>
                        <div style="position:absolute; bottom:3px;right:3px;font-size:30px; 
                                    width:40px; height:40px;" 
                            class="rounded-circle bg-primary">
                                <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#updateUserAvatar">
                                    <i style="font-size:24px; position: relative; left:8px; bottom:3px;" class="fas fa-camera-retro text-secondary"></i>
                                </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <div class="text-center m-2">
                    <div>{{$user->name ?? '' }}</div>
                    <div>{{$user->role_show ?? '' }}</div>
                </div>
            </div>
            <x:menu.leftbar :user="$user"/>
        </div>
        <div class="col-12 col-md-9 pt-4justify-content-center">
            <div class="row" data-masonry='{"percentPosition": true }'>

            <div class="col-12 col-lg-6 pb-2 pe-2">
                <div class="card shadow">
                    <div class="card-header pt-3 pb-2 d-flex justify-content-between">
                        <h6 class="m-0 font-weight-bold">ข้อมูลส่วนตัว</h6>
                        <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#profileEdit">
                            <i style="font-size:0.7em;" class="fas fa-edit"></i> แก้ไข
                        </a>
                    </div>
                    <div class="card-body">
                        @if(empty($user->profile))
                            ยังไม่มีข้อมูล กรุณากรอกข้อมูลโดยกดที่ปุ่มแก้ไข
                        @endif
                        @if(!empty($user->profile->firstname))
                            <div class="d-flex">
                                <div class="col-2">
                                    ชื่อ:
                                </div>
                                <div class="col-10">{{$user->profile->firstname ?? '' }} {{$user->profile->lastname ?? '' }}</div>
                            </div>
                        @endif 
                        @if(!empty($user->profile->nickname))
                            <div class="d-flex">
                                <div class="col-2">
                                    ชื่อเล่น:
                                </div> 
                                <div class="col-10">{{$user->profile->nickname ?? '' }}</div>
                            </div>
                        @endif 
                        @if(!empty($user->profile->phone))
                            <div class="d-flex">
                                <div class="col-2">
                                    โทร:
                                </div> 
                                <div class="col-10">
                                    {{$user->profile->phone ?? '' }}
                                </div>
                            </div>
                        @endif
                        @if(!empty($user->profile->gender))
                            <div class="d-flex">
                                <div class="col-2">เพศ:</div> 
                                <div class="col-10">
                                    @if(!empty($user->profile->gender))
                                        @switch($user->profile->gender)
                                            @case('male')
                                                ชาย
                                                @break
                                            @case('female')
                                                หญิง
                                                @break
                                            @default
                                                ไม่ระบุ
                                        @endswitch
                                    @endif
                                </div>
                            </div>
                        @endif
                        @if(!empty($user->profile->birth_year))
                            <div class="d-flex">
                                <div class="col-2">ปีเกิด:</div> 
                                <div class="col-10">{{$user->profile->birth_year ?? '' }}</div>
                            </div>
                        @endif
                        @if(!empty($user->profile->address))
                            <div class="d-flex">
                                <div class="col-2">ที่อยู่:</div> 
                                <div class="col-10">{{$user->profile->address ?? '' }}</div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            @if(user()->can('change_own_account_info'))
                <div class="col-12 col-lg-6 pb-2 pe-2">
                    <div class="card shadow">
                        <div class="card-header pt-3 pb-2 d-flex justify-content-between">

                        <h6 class="m-0 font-weight-bold">ข้อมูลบัญชี</h6>
                        <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#userEdit">
                            <i style="font-size:0.7em;" class="fas fa-edit"></i> แก้ไข
                        </a>
                        <div class="modal fade" id="userEdit" tabindex="-1" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title">ข้อมูลบัญชี</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              </div>
                              <div class="modal-body">
                                <form method="post" action="{{route('user.update',$user->slug)}}">
                                  @csrf
                                  <div class="d-flex mb-2">
                                    <div class="col-2">ชื่อแสดง:</div>
                                    <div class="col-10">
                                        <input type="text" name="name" class="form-control" placeholder="" value="{{$user->name ?? '' }}">
                                    </div>
                                  </div>
                                  <div class="d-flex mb-2">
                                    <div class="col-2">อีเมล:</div>
                                    <div class="col-10">
                                        <input type="text" name="email" class="form-control" placeholder="" value="{{$user->email ?? '' }}">
                                    </div>
                                  </div>
                                  <div id="change_password">
                                  </div>
                                <script type="text/javascript">
                                    $(function(){
                                        var change_password_request_form = '\
                                            <div class="d-flex">\
                                                <div class="col-2">รหัสผ่าน:</div> \
                                                <a class="text-primary" id="change_password_request" href="javascript:;">เปลี่ยนรหัสผ่าน</a>\
                                            </div>\
                                        ';
                                        $('#change_password').html(change_password_request_form);
                                        $('body').on('click','#change_password_request',function(){
                                            $('#change_password').html('\
                                                <div class="d-flex">\
                                                    <div class="user-edit-form col-2">รหัสผ่าน:</div> \
                                                    <div class="user-edit-form col-10">\
                                                        <input class="w-100 form-control mb-2" type="password" value="" name="password" />\
                                                    </div>\
                                                </div>\
                                                <div class="d-flex">\
                                                    <div class="user-edit-form col-2">ยืนยันรหัสผ่าน:</div> \
                                                    <div class="user-edit-form col-10">\
                                                        <input class="w-100 form-control mb-2" type="password" value="" name="password_confirmation" />\
                                                    </div>\
                                                </div>');
                                        });
                                        $('body').on('click tap','.user-edit-cancel',function(){
                                            $('#change_password').html(change_password_request_form);
                                        });
                                    });
                                </script>
                                @if(user()->can('manage_user'))
                                <div class="d-flex">
                                    <div class="col-2">สถานะ:</div> 
                                    <div class="user-edit-form">
                                        <div class="form-check d-inline-block">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" @if($user->status==1) checked @endif name="status" id="" value="1">
                                            ปกติ
                                            </label>
                                        </div>
                                        <div class="form-check d-inline-block">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" @if($user->status==0) checked @endif name="status" id="" value="0">
                                            ยกเลิก
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                  
                                </form>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>
                                <button type="button" class="btn btn-primary userEdit-submit">ส่งข้อมูล</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <script type="text/javascript">
                            $(function () {
                                $('body').on('click', '.userEdit-submit', function (e) {
                                    $('#userEdit').find('form').submit();
                                    $('#userEdit').modal('hide');
                                });
                            });
                        </script>  
                        </div>
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="col-2">ชื่อแสดง:</div> 
                                <div class="user-edit-show">{{$user->name ?? '' }}</div>
                            </div>
                            <div class="d-flex">
                                <div class="col-2">อีเมล:</div> 
                                <div class="user-edit-show">{{$user->email ?? '' }}</div>
                            </div>
                            @if(user()->can('manage_user'))
                            <div class="d-flex">
                                <div class="col-2">สถานะ:</div> 
                                <div class="user-edit-show">
                                    @if($user->status == 1)
                                        ปกติ
                                    @else
                                        <span class="text-danger">ยกเลิก</span>
                                    @endif
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endif
            @if(user()->can('connect_social'))
                <div class="col-12 col-lg-6 pb-2 pe-2">
                    <div class="card shadow">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold">เชื่อมต่อ Social</h6>
                        </div>
                        <div class="card-body">
                            <x-system::social own={{$own}}/>
                            <div>
                                <em style="font-size: 0.8em; color:gray;">* เมื่อเชื่อมต่อแล้วสามารถ Login เข้าบัญชีนี้โดยใช้ Social นั้นๆ ได้</em>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @if(user()->can('manage_user_role') && $user->getMaxPowerRole()->power <= user()->getMaxPowerRole()->power)
                <div class="col-12 col-lg-6 pb-2 pe-2">
                    <div class="card shadow">
                        <div class="card-header pt-3 pb-2 d-flex justify-content-between">
                            <h6 class="m-0 font-weight-bold">บทบาท</h6>
                        <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#roleEdit">
                            <i style="font-size:0.7em;" class="fas fa-edit"></i> แก้ไข
                        </a>
                        <x-user::modal.role 
                            :user="$user"
                        />
                        </div>
                        <div class="card-body">
                            @if(empty($user->role_show))
                                <div class="role-edit-show text-center">ผู้ใช้งานทั่วไป</div>
                            @endif
                            <div class="d-flex">
                                <div class="role-edit-form col-2" style="display: none;">บทบาท:</div> 
                                <div class="role-edit-show">{{$user->role_show ?? '' }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @if(user()->can('manage_user'))
                <div class="col-12 col-lg-6 pb-2 pe-2">
                    <div class="card shadow">
                        <div class="card-header pt-3 pb-2 d-flex justify-content-between">
                            Note (เห็นเฉพาะเจ้าหน้าที่)
                        </div>
                        <div class="card-body">
                            <script>
                                $(function(){
                                    $("#note_body").scrollTop($("#note_body")[0].scrollHeight);
                                });
                            </script> 
                            <div id="note_body" class="card-body" style="max-height: 200px; overflow: scroll;">
                            @foreach($user->notes()->get()->load(['user']) as $note)
                                <div class="d-flex mb-2">
                                    <div class="mt-2">
                                        <img style="width:36px;" class="rounded-circle" src="{{asset('/profile/show/avatar/'.$note->user->slug.'/sm/').'/'.$note->user->avatar}}"> 
                                    </div>
                                    <div class="ms-4 me-2">
                                        <div>
                                            {{$note->user->name}}
                                        </div>
                                        <div class="balloon balloon-start">
                                            {{$note->message}}
                                        </div> 
                                    </div>
                                    <div class="d-flex text-nowrap align-items-end">
                                        <div>{{vela()->thai($note->created_at->format('Y-m-d'))}}</div>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                            <form id="note-edit-form" method="post" action="{{route('user.note.insert',$user->slug)}}" class="needs-validation" novalidate>
                                @csrf
                                <div class="row mt-3">
                                    <div class="col-9 pe-0">
                                        <input type="text" class="form-control dark" name="message">
                                    </div>
                                    <div class="col-3">
                                        <input class="form-control btn btn-primary" type="submit" value="ส่งข้อมูล">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endif
        
            </div>
        </div>
    </div>
    <div class="pt-3 pb-3">
        <div>{{$linenotify ?? '' }}</div>
        <div>{{$line_at ?? '' }}</div>
    </div>
</div>
<x:modal.profile
    id="profileEdit"
    title="ข้อมูลส่วนตัว"
    :user='$user'
/>
@stop
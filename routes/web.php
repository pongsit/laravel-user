<?php

use Illuminate\Support\Facades\Route;
use Pongsit\User\Http\Controllers\UserController;

// user
Route::post('/user/update/{user:slug}', [UserController::class, 'update'])->name('user.update')->middleware('isLogin');
Route::middleware(['userCan:manage_user'])->group(function () {
    Route::get('/user', [UserController::class, 'index'])->name('user');
    Route::post('/user/search', [UserController::class, 'userSearch'])->name('user.search');
    Route::post('/user/role/update/{user:slug}', [UserController::class, 'updateUserRole'])->name('user.role.update');
    Route::post('/user/note/insert/{user:slug}', [UserController::class, 'noteInsert'])->name('user.note.insert');
    Route::get('/user/create', [UserController::class, 'create'])->name('user.create');
    Route::post('/user/store', [UserController::class, 'store'])->name('user.store');
    Route::get('/user/{user:slug}', [UserController::class, 'show'])->name('user.show');
});










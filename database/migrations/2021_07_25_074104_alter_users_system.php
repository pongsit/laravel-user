<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUsersSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('google_id')->nullable();
            $table->string('line_id')->nullable();
            $table->string('facebook_id')->nullable();
            $table->tinyInteger('status')->default('1');
            $table->string('avatar')->nullable();
            $table->string('slug')->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            if (Schema::hasColumn('users','google_id')) {
                $table->dropColumn('google_id');
            }
            if (Schema::hasColumn('users','google_id')) {
                $table->dropColumn('line_id');
            }
            if (Schema::hasColumn('users','google_id')) {
                $table->dropColumn('facebook_id');
            }
            if (Schema::hasColumn('users','status')) {
                $table->dropColumn('status');
            }
            if (Schema::hasColumn('users','avatar')) {
                $table->dropColumn('avatar');
            }
            if (Schema::hasColumn('users','slug')) {
                $table->dropColumn('slug');
            }
        });
    }
}
